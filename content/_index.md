# Welcome to this __Hugo__ starter page!
The aim with this is to be as simple as possible.  
Have fun with [*SCSS*](https://sass-lang.com/documentation/syntax) & Go templates! Typescript is not included.  
To get started, you can add animations, your own colors, or even make a blog section :)