# Hugo Template

## Description
This provides a template to build custom hugo sites on. It is not a traditional hugo template, and should *not* be copied to the template folder. Rather, this is more of a base to build on.

## Installation

### Dependancies
- [Git](https://git-scm.com/downloads) (On linux git is often there by default)
- [Hugo extended](https://gohugo.io/installation/)

### 1. Clone the Git repo
`git clone https://gitlab.com/SoftwareXYZ/hugo-template.git Template`  
(Replace the last "Template" with whatever you want the folder to be)

### 2. Start the Hugo server
`cd Template` (Or whatever you named the folder)  
`hugo server`...and the server should start!
## Support
If you need help, go to the [Hugo forums](https://discourse.gohugo.io/). If there's an issue or suggestion for the code itself, go to the issues tab and make an issue :()
## Contributing
To contribute, make a branch and merge request :)

## Project status
As I make more websites, I'll add more features to hopefully make this more useful to everyone else.